% Class for QPSK


classdef QPSK_CLASS
    properties
        nbp;       % Number of Bit Pairs
    end
    
    methods
        % ==================================================Constructor
        function [obj] = QPSK_CLASS(rmr_obj)
            obj.nbp = rmr_obj.E / rmr_obj.Q_m;
        end
        
        % ==================================================Modulate
        function [MOD_RATE_MAT_LDPC_EN_CODE_BLOCK] = modulate(obj, RATE_MAT_EN_CODE_BLOCK)
            c = 1/sqrt(2);      % Constant
            sym_1 = complex(c, c);         % Symbol 1
            sym_2 = complex(c, -c);        % Symbol 2
            sym_3 = complex(-c, c);        % Symbol 3
            sym_4 = complex(-c, -c);       % Symbol 4
            
            MOD_RATE_MAT_LDPC_EN_CODE_BLOCK = [];
            
            for i = 1 : obj.nbp
                bit_1 = RATE_MAT_EN_CODE_BLOCK((2 * i) - 1);
                bit_2 = RATE_MAT_EN_CODE_BLOCK((2 * i));
                sel_sym = complex(0, 0);        % Selected Symbol
                
                if((bit_1 == 0) && (bit_2 == 0))
                    sel_sym = sym_1;
                elseif((bit_1 == 0) && (bit_2 == 1))
                    sel_sym = sym_2;
                elseif((bit_1 == 1) && (bit_2 == 0))
                    sel_sym = sym_3;
                else
                    sel_sym = sym_4;
                end
                
                MOD_RATE_MAT_LDPC_EN_CODE_BLOCK = [MOD_RATE_MAT_LDPC_EN_CODE_BLOCK sel_sym];
            end
        end
        
        % ==================================================Demodulate
        function [DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK] = demodulate(obj, MOD_RATE_MAT_LDPC_EN_CODE_BLOCK)
            MOD_RATE_MAT_LDPC_EN_CODE_BLOCK__REAL = real(MOD_RATE_MAT_LDPC_EN_CODE_BLOCK);
            MOD_RATE_MAT_LDPC_EN_CODE_BLOCK__IMAG = imag(MOD_RATE_MAT_LDPC_EN_CODE_BLOCK);
            c = 1/sqrt(2);      % Constant
            
            DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK = [];      % Demodulated Rate Matched LDPC Encoded Code Block
            
            for i = 1 : obj.nbp
                real_part = MOD_RATE_MAT_LDPC_EN_CODE_BLOCK__REAL(i);
                llr0_real = abs(c + real_part);
                llr1_real = abs(-c + real_part);
                llr_real = log(llr0_real / llr1_real);
                
                imag_part = MOD_RATE_MAT_LDPC_EN_CODE_BLOCK__IMAG(i);
                llr0_imag = abs(c + imag_part);
                llr1_imag = abs(-c + imag_part);
                llr_imag = log(llr0_imag / llr1_imag);
                
                DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK = [DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK llr_real llr_imag];
            end
        end
    end
end