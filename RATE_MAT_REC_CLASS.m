% Class for Rate Matching & Rate Recovery


classdef RATE_MAT_REC_CLASS
    properties
        Q_m;        % Modulation Order
        N_Pilot_REs;        % Number of Pilot Resource Elements
        N_PRB;      % Number of Physical Resource Blocks
        G;      % Number of Tx Bits
        E;      % Length of a Rate Matched Code Block
    end
    
    methods
        % ==================================================Constructor
        function [obj] = RATE_MAT_REC_CLASS(C)
            obj.Q_m = 2;        % For I_MCS = 9
            obj.N_PRB = 100;        % Given
            obj.N_Pilot_REs = 6;        % Given
            
            obj.G = obj.Q_m * ((14 * 12) - obj.N_Pilot_REs) * obj.N_PRB;
            fprintf("\nG = %d\n", obj.G);
            
            obj.E = obj.G / C;
            fprintf("E = %d\n", obj.E);
        end
        
        % ==================================================Match Rate
        function [RATE_MAT_LDPC_EN_CODE_BLOCK] = rate_match(rmr_obj, LDPC_EN_CODE_BLOCK, rv_id)
            if(rv_id == 0)
                k_0 = 0;        % Start Index For Base Graph 1
            end
            
            N = size(LDPC_EN_CODE_BLOCK, 2);     % Length of Encoded Code Block
            j = 1;      % Read Index
            k = 1;      % Write Index
            
            while(k <= rmr_obj.E)
                read_index = mod((k_0 + j), N);     % Assume N_cb = N
                
                if(LDPC_EN_CODE_BLOCK(read_index) ~= -1)     % Ignore Fillers (-1)
                    RATE_MAT_LDPC_EN_CODE_BLOCK(k) = LDPC_EN_CODE_BLOCK(read_index);
                    k = k + 1;
                end
                
                j = j + 1;
            end
        end
        
        % ==================================================Recover Rate
        function [RATE_REC_LDPC_EN_CODE_BLOCK] = rate_recover(rmr_obj, DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK, fil_start_index, fil_len, N, rv_id)
            if(rv_id == 0)
                k_0 = 0;        % Start Index For Base Graph 1
            end
            
            RATE_REC_LDPC_EN_CODE_BLOCK = [];
            RATE_REC_LDPC_EN_CODE_BLOCK = [RATE_REC_LDPC_EN_CODE_BLOCK DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK(1 : (fil_start_index - 1))];
            %fillers = -1 * ones(1, fil_len);
            fillers = zeros(1, fil_len);
            RATE_REC_LDPC_EN_CODE_BLOCK = [RATE_REC_LDPC_EN_CODE_BLOCK fillers];
            RATE_REC_LDPC_EN_CODE_BLOCK = [RATE_REC_LDPC_EN_CODE_BLOCK DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK(fil_start_index : rmr_obj.E)];
            RATE_REC_LDPC_EN_CODE_BLOCK = [RATE_REC_LDPC_EN_CODE_BLOCK zeros(1, (N - rmr_obj.E - fil_len))];
        end
    end
end