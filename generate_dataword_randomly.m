% Generate A Dataword Randomly


function [DATAWORD] = generate_dataword_randomly(K)
    DATAWORD = randi([0 1], 1, K);
end