% Rx Chain


function [CRC_DE_TRANSPORT_BLOCK_Rx] = receive(A, SELECTED_GENERATOR, QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, fillers_start_index, nbg, nldpcdecits)
    crc_obj_Rx = CRC_CLASS(A, SELECTED_GENERATOR);      % Object of CRC_CLASS
    seg_con_obj_Rx = SEG_CON_CLASS(crc_obj_Rx);        % Object of SEG_CON_CLASS
    rmr_obj_Rx = RATE_MAT_REC_CLASS(seg_con_obj_Rx.C);      % Object of RATE_MAT_REC_CLASS
    
    % ================================================== 1. QPSK Demodulation : START
    qpsk_obj_Rx = QPSK_CLASS(rmr_obj_Rx);        % Object of QPSK_CLASS

    QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX = [];

    for i = 1 : seg_con_obj_Rx.C
        QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCK = QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX(:, i);
        [QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK] = qpsk_obj_Rx.demodulate(transpose(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCK));
        QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX = [QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX transpose(QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK)];
    end

    fprintf("\nQPSK Demodulated Rate Matched LDPC Encoded Code Blocks Matrix (%d x %d) = \n", size(QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, 1), size(QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, 2));
    %disp(QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX);
    % ================================================== 1. QPSK Demodulation : END

    % ================================================== 2. Rate Recovery : START
    RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX = [];

    for i = 1 : seg_con_obj_Rx.C
        QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK = QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX(:, i);
        [RATE_REC_LDPC_EN_CODE_BLOCK] = rmr_obj_Rx.rate_recover(transpose(QPSK_DEMOD_RATE_MAT_LDPC_EN_CODE_BLOCK), fillers_start_index, seg_con_obj_Rx.F, 3 * seg_con_obj_Rx.K, 0);
        RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX = [RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX transpose(RATE_REC_LDPC_EN_CODE_BLOCK)];
    end

    fprintf("\nRate Recovered LDPC Encoded Code Blocks Matrix (%d x %d) = \n", size(RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX, 1), size(RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX, 2));
    %disp(RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX);
    % ================================================== 2. Rate Recovery : END

    % ==================== 3. LDPC Decoding : START
    LDPC_DE_CODE_BLOCKS_MATRIX = double(LDPCDecode(RATE_REC_LDPC_EN_CODE_BLOCKS_MATRIX, nbg, nldpcdecits));
    LDPC_DE_CODE_BLOCKS_MATRIX((seg_con_obj_Rx.K - seg_con_obj_Rx.F + 1):seg_con_obj_Rx.K, 1:seg_con_obj_Rx.C) = -1 * ones(seg_con_obj_Rx.F, seg_con_obj_Rx.C);     % Modifying Fillers

    fprintf("\nLDPC Decoded Code Blocks Matrix (%d x %d) = \n", size(LDPC_DE_CODE_BLOCKS_MATRIX, 1), size(LDPC_DE_CODE_BLOCKS_MATRIX, 2));
    %disp(LDPC_DE_CODE_BLOCKS_MATRIX);
    % ==================== 3. LDPC Decoding : END
    
    MODIFIED_FILLED_LDPC_DE_CODE_BLOCKS_MATRIX = transpose(LDPC_DE_CODE_BLOCKS_MATRIX);

    % ================================================== 4. Concatenation : START
    [CRC_EN_TRANSPORT_BLOCK_Rx] = seg_con_obj_Rx.concat(A, MODIFIED_FILLED_LDPC_DE_CODE_BLOCKS_MATRIX);
    % ================================================== 4. Concatenation : END

    % ================================================== 5. CRC Decoding : START
    [decision, CRC_DE_TRANSPORT_BLOCK_Rx] = crc_obj_Rx.crc_decoder(CRC_EN_TRANSPORT_BLOCK_Rx, A);

    if decision == 0
        disp("0");
    else
        disp("1");
        fprintf("Received Transport Block = \n");
        %disp(CRC_DE_TRANSPORT_BLOCK_Rx);
    end
    % ================================================== 5. CRC Decoding : END

end