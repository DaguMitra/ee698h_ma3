% Tx Chain


function [fillers_start_index, QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX] = transmit(A, SELECTED_GENERATOR, TRANSPORT_BLOCK, nbg)
    % ================================================== 1. CRC Encoding : START
    crc_obj_Tx = CRC_CLASS(A, SELECTED_GENERATOR);      % Object of CRC_CLASS

    [CRC_EN_TRANSPORT_BLOCK] = crc_obj_Tx.crc_encoder(TRANSPORT_BLOCK);
    fprintf("\nCRC Encoded Transport Block (%d b) = \n", crc_obj_Tx.B);
    %disp(CRC_EN_TRANSPORT_BLOCK);
    % ================================================== 1. CRC Encoding : END
    
    % ================================================== 2. Segmentation : START
    seg_con_obj_Tx = SEG_CON_CLASS(crc_obj_Tx);      % Object of SEG_CON_CLASS

    [CODE_BLOCKS_MATRIX] = seg_con_obj_Tx.segment(CRC_EN_TRANSPORT_BLOCK);
    fprintf("\nCode Blocks Matrix (%d x %d) = \n", size(CODE_BLOCKS_MATRIX, 1), size(CODE_BLOCKS_MATRIX, 2));
    %disp(CODE_BLOCKS_MATRIX);
    % ================================================== 2. Segmentation : END
    
    % ================================================== 3. LDPC Encoding : START
    TRANSPOSED_CODE_BLOCKS_MATRIX = transpose(CODE_BLOCKS_MATRIX);      % Transpose of CODE_BLOCKS_MATRIX
    [LDPC_EN_CODE_BLOCKS_MATRIX] = double(LDPCEncode(TRANSPOSED_CODE_BLOCKS_MATRIX, nbg));
    fprintf("\nLDPC Encoded Code Blocks Matrix (%d x %d) = \n", size(LDPC_EN_CODE_BLOCKS_MATRIX, 1), size(LDPC_EN_CODE_BLOCKS_MATRIX, 2));
    %disp(LDPC_EN_CODE_BLOCKS_MATRIX);
    % ================================================== 3. LDPC Encoding : END

    fillers_start_index = 0;        % Starting Index of Filler Bits

    for i = 1:(3 * seg_con_obj_Tx.K)
        if((LDPC_EN_CODE_BLOCKS_MATRIX(i, 1) < 0) && (fillers_start_index == 0))
            fillers_start_index = i;
        end
    end

    fprintf("\nfillers_start_index = %d\n", fillers_start_index);    
    
    % ================================================== 4. Rate Matching : START
    rmr_obj_Tx = RATE_MAT_REC_CLASS(seg_con_obj_Tx.C);      % Object of RATE_MAT_REC_CLASS
    RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX = [];

    for i = 1 : seg_con_obj_Tx.C
        LDPC_EN_CODE_BLOCK = LDPC_EN_CODE_BLOCKS_MATRIX(:, i);
        [RATE_MAT_LDPC_EN_CODE_BLOCK] = rmr_obj_Tx.rate_match(transpose(LDPC_EN_CODE_BLOCK), 0);
        RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX = [RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX transpose(RATE_MAT_LDPC_EN_CODE_BLOCK)];
    end

    fprintf("\nRate Matched LDPC Encoded Code Blocks Matrix (%d x %d) = \n", size(RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, 1), size(RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, 2));
    %disp(RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX);
    % ================================================== 4. Rate Matching : END

    % ================================================== 5. QPSK Modulation : START
    qpsk_obj_Tx = QPSK_CLASS(rmr_obj_Tx);        % Object of QPSK_CLASS

    QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX = [];

    for i = 1 : seg_con_obj_Tx.C
        RATE_MAT_LDPC_EN_CODE_BLOCK = RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX(:, i);
        [QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCK] = qpsk_obj_Tx.modulate(transpose(RATE_MAT_LDPC_EN_CODE_BLOCK));
        QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX = [QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX transpose(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCK)];
    end

    fprintf("\nQPSK Modulated Rate Matched LDPC Encoded Code Blocks Matrix (%d x %d) = \n", size(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, 1), size(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, 2));
    %disp(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX);
    % ================================================== 5. QPSK Modulation : END
end