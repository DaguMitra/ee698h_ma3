% Class for Segmentation & Concatenation


classdef SEG_CON_CLASS
    properties
        K_CB = 8448;        % Maximum Length of a Code Block
        GENERATOR_2 = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1];      % CRC24B as per the standard
        L;      % Length of a Code Block CRC
        C;      % Number of Code Blocks
        B_PRIME;        % Length of a Effective Payload
        K_PRIME;        % Minimum Length of a Code Block
        K_b = 22;        % Number of Symmetric Columns in Base Graph 1
        LS_MATRIX = [2 4 8 16 32 64 128 256;
                    3 6 12 24 48 96 192 384;
                    5 10 20 40 80 160 320 0;
                    7 14 28 56 112 224 0 0;
                    9 18 36 72 144 288 0 0;
                    11 22 44 88 176 352 0 0;
                    13 26 52 104 208 0 0 0;
                    15 30 60 120 240 0 0 0];      % Lifting Sizes Matrix
        Z_C = 1;        % Lifting Size
        K;        % Length of a Code Block
        F;      % Length of Filler Bits
        cb_data_len;        % Length of Code Block Data
    end
    
    methods
        % ==================================================Constructor
        function [obj] = SEG_CON_CLASS(crc_obj)
            if(crc_obj.B <= obj.K_CB)
                obj.L = 0;
                obj.C = 1;
            else
                obj.L = size(obj.GENERATOR_2, 2) - 1;
                obj.C = ceil(crc_obj.B/(obj.K_CB - obj.L));
            end            
            
            fprintf("\nL = %d\n", obj.L);
            fprintf("C = %d\n", obj.C);
            obj.B_PRIME = crc_obj.B + (obj.C * obj.L);
            fprintf("B_PRIME = %d\n", obj.B_PRIME);
            obj.K_PRIME = uint64(obj.B_PRIME / obj.C);
            fprintf("K_PRIME = %d\n", obj.K_PRIME);
            mat_dim = size(obj.LS_MATRIX, 1);       % Dimension of LS_MATRIX

            for i = 1 : mat_dim       % Find the Min Z_C
                for j = 1 : mat_dim
                    ls = obj.LS_MATRIX(i, j);       % Lifting Size

                    if((obj.K_b * ls) >= obj.K_PRIME)
                        if(obj.Z_C == 1)
                            obj.Z_C = ls;
                        else
                            if(obj.Z_C > ls)
                                obj.Z_C = ls;
                            end
                        end
                    end
                end
            end

            fprintf("Z_C = %d\n", obj.Z_C);
            obj.K = obj.K_b * obj.Z_C;
            fprintf("K = %d\n", obj.K);
            obj.F = obj.K - obj.K_PRIME;
            fprintf("F = %d\n", obj.F);
            obj.cb_data_len = obj.K_PRIME - obj.L;     % Length of Code Block Data
            fprintf("cb_data_len = %d\n", obj.cb_data_len);
        end
        
        % ==================================================Segment
        function [FILLED_EN_CODE_BLOCKS_MATRIX] = segment(obj, EN_TRANSPORT_BLOCK)
            crc_obj2 = CRC_CLASS(obj.cb_data_len, obj.GENERATOR_2);      % Object of CRC_CLASS
            FILLED_EN_CODE_BLOCKS_MATRIX = [];     % Matrix of Filled Encoded Code Blocks

            for i = 1:obj.C
                cb_data = EN_TRANSPORT_BLOCK(((i - 1) * obj.cb_data_len) + 1: (i * obj.cb_data_len));       % Code Block Data
                fprintf("\nCode Block %d Data = \n", i);
                %disp(cb_data);
                en_cb_data = [];        % Encoded Code Block Data
                
                if(obj.C > 1)                    
                    [en_cb_data] = crc_obj2.crc_encoder(cb_data);
                    fprintf("CRC Encoded Code Block %d Data = \n", i);
                    %disp(en_cb_data);
                end
                
                filled_en_cb_data = en_cb_data;     % Filled Encoded Code Block Data
                filled_en_cb_data((obj.K_PRIME + 1):obj.K) = -1 + zeros(1, obj.F);
                fprintf("Filled CRC Encoded Code Block %d Data = \n", i);
                %disp(filled_en_cb_data);
                
                FILLED_EN_CODE_BLOCKS_MATRIX = [FILLED_EN_CODE_BLOCKS_MATRIX; filled_en_cb_data];
            end
        end
        
        % ==================================================Concat
        function [EN_TRANSPORT_BLOCK] = concat(obj, A, MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX)
            B = A + 24;     % Length of Encoded Transport Block
            
            crc_obj2 = CRC_CLASS(obj.cb_data_len, obj.GENERATOR_2);      % Object of CRC_CLASS
            EN_TRANSPORT_BLOCK = [];     % Encoded Transport Block

            for i = 1:obj.C
                en_cb_data = MODIFIED_FILLED_EN_CODE_BLOCKS_MATRIX(i,1:obj.K_PRIME);       % Encoded Code Block Data                
                fprintf("\nEncoded Code Block %d Data = \n", i);
                %disp(en_cb_data);
                
                [decision, cb_data] = crc_obj2.crc_decoder(en_cb_data, obj.cb_data_len);      % Code Block Data
                
                if decision == 0
                    disp("0");
                else
                    disp("1");
                    fprintf("Code Block %d Data = \n", i);
                    %disp(cb_data);
                end
                
                EN_TRANSPORT_BLOCK(((i - 1) * obj.cb_data_len) + 1:(i * obj.cb_data_len)) = cb_data;     % Concatenation
            end
        end
    end
end