clc;
clear all;

A = 20496;      % Length of a Transport Block (b)
[TRANSPORT_BLOCK] = generate_dataword_randomly(A);
fprintf("Generated Transport Block (%d b) = \n", A);
%disp(TRANSPORT_BLOCK);

SELECTED_GENERATOR = [];      % Selected Generator
GENERATOR_1 = [1 1 0 0 0 0 1 1 0 0 1 0 0 1 1 0 0 1 1 1 1 1 0 1 1];      % CRC24A as per the standard
%{
GENERATOR_2 = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1];      % CRC24B as per the standard
GENERATOR_3 = [1 1 0 1 1 0 0 1 0 1 0 1 1 0 0 0 1 0 0 0 1 0 1 1 1];      % CRC24C as per the standard

fprintf("Generator 1 = \n");
disp(GENERATOR_1);
fprintf("Generator 2 = \n");
disp(GENERATOR_2);
fprintf("Generator 3 = \n");
disp(GENERATOR_3);

generator_choice = input('Select a 24 bits generator (1 / 2 / 3 / DEFAULT : 1): ');

switch generator_choice
    case 1
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_1];
    case 2
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_2];
    case 3
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_3];
    otherwise
        SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_1];
end
%}
SELECTED_GENERATOR = [SELECTED_GENERATOR; GENERATOR_1];

nbg = 1;        % Base Graph Number
nldpcdecits = 25;       % Maximum Number of Iterations

fprintf("\n==========Transmitter==========\n");
[fillers_start_index, QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX] = transmit(A, SELECTED_GENERATOR, TRANSPORT_BLOCK, nbg);

% ================================================== Channel : START

noise_power = (10 ^ -5);
real_part = randn(size(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX));
imag_part = randn(size(QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX));
NOISE = sqrt(noise_power) * (1 / sqrt(2)) * (real_part + imag_part * 1i);

RX_SIG = QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX + NOISE;

% ================================================== Channel : END

fprintf("\n==========Receiver==========\n");
%[CRC_DE_TRANSPORT_BLOCK_Rx] = receive(seg_con_obj_Tx, QPSK_MOD_RATE_MAT_LDPC_EN_CODE_BLOCKS_MATRIX, fillers_start_index, nbg, nldpcdecits, A, SELECTED_GENERATOR);     % For Noiseless Channel
[CRC_DE_TRANSPORT_BLOCK_Rx] = receive(A, SELECTED_GENERATOR, RX_SIG, fillers_start_index, nbg, nldpcdecits);

% ================================================== Analysis
fprintf("\n==========Analysis==========\n");
ERRORS = find(CRC_DE_TRANSPORT_BLOCK_Rx - TRANSPORT_BLOCK);
[r, c] = size(ERRORS);

if((r == 1) && (c == 0))
    fprintf("\nIncorrupted Transport Block.\n");
else
    fprintf("\nCorrupted Transport Block!\n");
end